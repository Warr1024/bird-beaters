local samplerate = 48000
local depth = 16

local function gen(segments)
	local total = 0
	for _, seg in ipairs(segments) do
		seg[1] = math.floor(seg[1] * samplerate)
		total = total + seg[1]
	end
	local data = love.sound.newSoundData(total, samplerate, depth, 1)
	local time = 0
	local pos = 0
	local val = 0
	for _, seg in pairs(segments) do
		for _ = 1, seg[1] do
			time = time + seg[2] / samplerate
			if time >= 1 then time = time - 1 end
			val = val * 0.90 + (time >= 0.5 and 1 or -1) * 0.1
			data:setSample(pos, val)
			pos = pos + 1
		end
	end
	return function()
		local src = love.audio.newSource(data)
		src:play()
		return src
	end
end

local function frq(n) return 440 * 2 ^ (n / 12) end

local soundfx = {}

local myseg = {}
for n = 1, 3 do myseg[#myseg + 1] = {1/50, frq(n - 48)} end
soundfx.flap = gen(myseg)

myseg = {}
for n = 1, 4 do myseg[#myseg + 1] = {1/50, frq(n - 24)} end
for n = 1, 4 do myseg[#myseg + 1] = {1/50, frq(-n - 24)} end
soundfx.throw = gen(myseg)

myseg = {}
for n = 1, 12 do myseg[#myseg + 1] = {1/100, frq(n * 2 + 24)} end
soundfx.egghit = gen(myseg)

myseg = {}
for n = 1, 12 do myseg[#myseg + 1] = {1/200, frq(-n * 2 + 12)} end
soundfx.birdhit = gen(myseg)

myseg = {}
for n = 1, 12 do myseg[#myseg + 1] = {1/20, frq(-n * 2 + 12)} end
soundfx.fail = gen(myseg)

myseg = {}
for n = 1, 12 do myseg[#myseg + 1] = {1/100, frq(n * 2 + 24)} end
for n = 1, 12 do myseg[#myseg + 1] = {1/100, frq(-n * 2 + 36)} end
for n = 1, 12 do myseg[#myseg + 1] = {1/100, frq(n * 2 + 24)} end
soundfx.win = gen(myseg)

return soundfx