function love.conf(t)
	t.identity = 'bird-beaters'
	t.version = "11.2"
	t.window.title = string.format("Bird Beaters - LÖVE %d.%d.%d",
		love.getVersion())

	t.window.width = 1024
	t.window.height = 576
	t.window.resizable = true
	t.window.minwidth = 320
	t.window.minheight = 200
	t.window.vsync = 1
	t.window.highdpi = false
	t.window.usedpiscale = true

	t.modules.audio = true
	t.modules.data = false
	t.modules.event = true
	t.modules.font = true
	t.modules.graphics = true
	t.modules.image = true
	t.modules.joystick = true
	t.modules.keyboard = true
	t.modules.math = false
	t.modules.mouse = false
	t.modules.physics = false
	t.modules.sound = true
	t.modules.system = false
	t.modules.thread = false
	t.modules.timer = true
	t.modules.touch = false
	t.modules.video = false
	t.modules.window = true
end