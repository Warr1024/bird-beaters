local images = {}
for _, fn in pairs(love.filesystem.getDirectoryItems("")) do
	if fn:match("%.png$") and love.filesystem.getInfo(fn, "file") then
		images[fn:gsub("%.png$", "")] = love.graphics.newImage(fn)
	end
end
return images