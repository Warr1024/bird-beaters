local controls = {}

local world = require("world")

local thrust = 0.5
local upthrust = 1.5
local birdkeys = {}
function birdkeys.up()
	return world.bird:flap(0, -upthrust)
end
function birdkeys.down()
	return world.bird:flap(0, thrust)
end
function birdkeys.left()
	return world.bird:flap(-thrust, 0)
end
function birdkeys.right()
	return world.bird:flap(thrust, 0)
end
function birdkeys.space()
	return world.bird:throw()
end
birdkeys["return"] = function()
	return world.bird:throw(true)
end

function controls.keypressed(key)
	if key == "escape" then
		love.event.quit()
	elseif key == "`" then
		world.cgablue = 1 - world.cgablue
	elseif birdkeys[key] then
		world.controlaction = birdkeys[key]
	end
end

local padkeys = {
	dpup = "up",
	dpdown = "down",
	dpright = "right",
	dpleft = "left",
	a = "space",
	b = "space",
	x = "space",
	y = "space",
	leftshoulder = "return",
	rightshoulder = "return",
	guide = "escape"
}
function controls.gamepadpressed(_, button)
	if padkeys[button] then return love.keypressed(padkeys[button]) end
end

return controls