# Bird Beaters

A spiritual remake of the very first complete game I wrote entirely from scratch, using ~1500 lines of QBASIC 1.0 on my 286 (10MHz 640k) with a CGA (320x200 4-color) display. No surviving copies of the original remain, so this is recreated largely from memory