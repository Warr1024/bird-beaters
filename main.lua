local ceil = math.ceil
local floor = math.floor
local random = math.random

local world = require("world")
local controls = require("controls")

love.keypressed = controls.keypressed
love.gamepadpressed = controls.gamepadpressed

do
	local resid = 0
	local ticksize = 1/120
	function love.update(dt)
		resid = resid + dt
		while resid > ticksize do
			world:tick(ticksize)
			resid = resid - ticksize
		end
	end
end

local function noisetile()
	local data = love.image.newImageData(128, 128)
	data:mapPixel(function()
		local x = random() * random()
		return x, x, x, 1
	end)
	return love.graphics.newImage(data)
end
local oldtile = noisetile()
local newtile = noisetile()
local tiletime = 0

local cga = love.graphics.newCanvas(world.bounds.x, world.bounds.y)
cga:setFilter("nearest", "nearest", 1)
local upscaled
function love.draw()
	cga:renderTo(function() return world:draw() end)
	love.graphics.setColor(1, 1, 1, 1)

	local w = love.graphics.getWidth()
	local h = love.graphics.getHeight()
	local iw = ceil(w / world.bounds.x)
	local ih = ceil(h / world.bounds.y)
	local i = iw < ih and iw or ih

	local usize = world.bounds * i
	if (not upscaled) or (upscaled:getWidth() ~= usize.x)
	or (upscaled:getHeight() ~= usize.y) then
		upscaled = love.graphics.newCanvas(usize.x, usize.y)
	end
	upscaled:renderTo(function()
		love.graphics.clear()
		love.graphics.draw(cga, 0, 0, 0, i)
	end)

	do
		local now = love.timer.getTime() / 4
		local function tile(t)
			for y = 0, h, t:getWidth() do
				for x = 0, w, t:getHeight() do
					love.graphics.draw(t, x, y, 0)
				end
			end
		end
		local alpha = now - tiletime
		if alpha >= 1 then
			alpha = alpha - floor(alpha)
			tiletime = floor(now)
			oldtile = newtile
			newtile = noisetile()
		end
		tile(oldtile)
		love.graphics.setColor(1, 1, 1, alpha)
		tile(newtile)
	end


	local sw = (w - 32) / usize.x
	local sh = (h - 32) / usize.y
	local s = sw < sh and sw or sh
	local cx = floor((w - usize.x * s) / 2)
	local cy = floor((h - usize.y * s) / 2)
	local cw = ceil(usize.x * s)
	local ch = ceil(usize.y * s)
	love.graphics.setColor(1, 1, 1, 0.1)
	for n = 1, 16, 2 do
		love.graphics.rectangle("fill",
			cx - n, cy - n,
			cw + n * 2, ch + n * 2
		)
	end
	love.graphics.setColor(1, 1, 1, 1)
	love.graphics.draw(upscaled, cx, cy, 0, s)
end
