local vec2d = require("vec2d")

local physobj = {
	pos = vec2d(),
	vel = vec2d(),
	size = vec2d(),
	grav = vec2d({y = 0.02}),
	face = 1
}
local physobjmeta = {__index = physobj}

setmetatable(physobj, {
	__call = function(_, t)
		t = t or {}
		setmetatable(t, physobjmeta)
		return t
	end
})

function physobj:tick(dt)
	self.vel = (self.vel + self.grav) * 0.99
	self.pos = self.pos + self.vel
	self.face = self.vel.x < 0 and -1 or 1
	if self.nobounds then return end
	local top = self.pos - self.size / 2
	local bot = self.pos + self.size / 2
	for a, b in pairs({y = "x", x = "y"}) do
		if top[a] < 0 and self.vel[a] < 0 then
			self.pos = vec2d({
				[a] = self.size[a] / 2,
				[b] = self.pos[b]
			})
			self.vel  = vec2d({
				[a] = -self.vel[a],
				[b] = self.vel[b]
			})
		elseif bot[a] > self.world.bounds[a] and self.vel[a] > 0 then
			self.pos = vec2d({
				[a] = self.world.bounds[a] - self.size[a] / 2,
				[b] = self.pos[b]
			})
			self.vel = vec2d({
				[a] = -self.vel[a],
				[b] = self.vel[b]
			})
		end
	end
	if self.frame and self.frame.ttl then
		self.frame.ttl = self.frame.ttl - dt
		if self.frame.ttl <= 0 then
			self.frame = self.frame.next and self.frame.next()
		end
	end
end

local floor = math.floor
function physobj:draw()
	local img = self.frame and self.frame.img or self.img
	if type(img) == "function" then img = img() end
	if self.vel.x < 0 then
		love.graphics.draw(img,
			floor(self.pos.x + self.size.x / 2),
			floor(self.pos.y - self.size.y / 2),
			0, -1, 1
		)
	else
		love.graphics.draw(img,
			floor(self.pos.x - self.size.x / 2),
			floor(self.pos.y - self.size.y / 2)
		)
	end
end

function physobj:collide(peer, elast)
	local avgsize = (self.size + peer.size) / 2
	local maxdist = ((avgsize.x + avgsize.y) / 2) ^ 2
	local diff = self.pos - peer.pos
	local dsqr = diff:lsqr()
	if dsqr < 1 then dsqr = 1 end
	if dsqr < maxdist then
		local dv = diff / dsqr * elast
		self.vel = self.vel + dv
		peer.vel = peer.vel - dv
		return 1 / dsqr
	end
end

return physobj