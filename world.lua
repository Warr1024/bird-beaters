local rand = math.random

local soundfx = require("soundfx")
local images = require("images")
local vec2d = require("vec2d")
local physobj = require("physobj")

local world = {
	bounds = vec2d(320, 200),
	level = 1,
	cgablue = 1
}

world.bird = physobj({
	world = world,
	health = 1,
	hurttime = 0,
	seekers = 1,
	img = images.bird_idle,
	vel = vec2d(-0.01, 0),
	size = vec2d(32, 32),
	pos = vec2d(240, 100)
})

world.egg = physobj({
	world = world,
	damage = 0,
	sleepends = 0,
	size = vec2d(16, 16),
	pos = vec2d(120, 100)
})
world.egg.img = function() return images["egg_" .. world.egg.damage] end

local flaptime = 0.15
function world.bird:throw(seeker)
	if self.frame or self.health <= 0 or world.feather
	or seeker and self.seekers < 1 then return end
	local t = {
		world = world,
		img = images.feather_normal,
		size = vec2d(16, 16),
		pos = vec2d(
			self.pos.x + self.face
			* self.size.x * 0.45,
			self.pos.y - self.size.y * 0.45
		),
		vel = vec2d(
			self.vel.x + self.face,
			self.vel.y - 1
		),
		face = self.face,
		nobounds = not seeker,
		seeker = seeker,
		grav = seeker and vec2d()
	}
	if seeker then
		local on, off
		on = function() return {
			img = images.feather_normal,
			ttl = 0.1,
			next = off
		} end
		off = function() return {
			img = images.feather_inverse,
			ttl = 0.1,
			next = on
		} end
		t.frame = on()
	end
	self.frame = {
		img = images.bird_throw,
		ttl = flaptime
	}
	soundfx.flap()
	if seeker then self.seekers = self.seekers - 1 end
	world.feather = physobj(t)
end
function world.bird:flap(x, y)
	if self.frame or self.health <= 0 then return end
	self.frame = {
		img = images.bird_flap,
		ttl = flaptime
	}
	soundfx.flap()
	self.vel = self.vel + vec2d(x, y)
end

function world:tick(dt)
	if self.diesound and not self.diesound:isPlaying() then
		return love.event.quit()
	end
	if self.bird.health <= 0 then return end

	if self.controlaction then self.controlaction(self) end
	self.controlaction = nil

	do
		local now = love.timer.getTime()
		if now > self.egg.sleepends then
			self.egg.sleepends = now + 2
			self.egg.vel = self.egg.vel + vec2d(
				(rand() - 0.5) * self.level,
				(rand() - 0.5) * self.level
			)
		end
	end

	if self.feather and self.feather.seeker then
		local dir = self.egg.pos - self.feather.pos
		local dist = dir:len()
		if dist > 0 then
			local seekrate = 0.1
			dir = dir / dist
			self.feather.vel = self.feather.vel + (dir * seekrate)
		end
	end

	local birdhit = self.bird:collide(self.egg, 5)
	if birdhit then
		self.bird.health = self.bird.health - birdhit * 5
		if self.bird.health <= 0 then
			self.diesound = soundfx.fail()
		else
			local now = love.timer.getTime()
			if self.bird.hurttime <= now then
				soundfx.birdhit()
				self.bird.hurttime = now + 0.1
			end
		end
	end

	if self.feather and self.egg:collide(self.feather, 10) then
		self.egg.damage = self.egg.damage + (self.feather.seeker and 5 or 2)
		if self.egg.damage > 4 then
			self.level = self.level + 1
			if (self.level - 1) % 3 == 0 and self.bird.seekers < 3 then
				self.bird.seekers = self.bird.seekers + 1
			end
			self.egg.damage = 0
			soundfx.win()
		else
			soundfx.egghit()
		end
		self.feather = nil
	end

	if self.feather then self.feather:tick(dt) end
	self.bird:tick(dt)
	self.egg:tick(dt)

	if self.feather and self.feather.pos.y > self.bounds.y
	+ self.feather.size.y / 2 then
		self.feather = nil
	end
end

function world:draw()
	love.graphics.clear(0, 0, 0, 1)
	love.graphics.setColor(1, 1, self.cgablue, 1)
	self.bird:draw()
	self.egg:draw()
	if self.feather then self.feather:draw() end

	love.graphics.setColor(1, 1/3, self.cgablue, 1)
	love.graphics.rectangle("fill", 0, 0,
		self.bounds.x * self.bird.health, 3)

	love.graphics.setColor(1, 1, self.cgablue, 1)
	for x = 1, self.bird.seekers do
		love.graphics.draw(images.feather_inverse, (x - 1) * 16, 0)
	end
	local textw = love.graphics.getFont():getWidth(self.level)
	love.graphics.print(self.level,
		self.bounds.x - textw - 2, 1)
end

return world