local vec2d = {x = 0, y = 0}
local vec2dmeta = {__index = vec2d}

setmetatable(vec2d, {
	__call = function(_, x, y)
		local t = type(x) == "table"
		and x or {x = x, y = y} or {}
		setmetatable(t, vec2dmeta)
		return t
	end
})

function vec2dmeta:__add(peer)
	return vec2d(self.x + peer.x, self.y + peer.y)
end
function vec2dmeta:__sub(peer)
	return vec2d(self.x - peer.x, self.y - peer.y)
end
function vec2dmeta:__mul(scale)
	return vec2d(self.x * scale, self.y * scale)
end
function vec2dmeta:__div(scale)
	return vec2d(self.x / scale, self.y / scale)
end
function vec2dmeta:__unm()
	return vec2d(-self.x, -self.y)
end
function vec2dmeta:__eq(peer)
	return self.x == peer.x and self.y == peer.y
end
function vec2dmeta:__tostring()
	return "(" .. self.x .. "," .. self.y .. ")"
end

function vec2d:lsqr()
	return self.x ^ 2 + self.y ^ 2
end
function vec2d:len()
	return (self.x ^ 2 + self.y ^ 2) ^ 0.5
end

return vec2d